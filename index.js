// Value divisible by 5, 10.
let number = Number(prompt('Give me number'));
console.log('The number you provided is ' + number + ".")

for (let count = number; count >=0; count--) {
	if (count <= 50) {
		console.log('The current value is at 50. Terminating the loop.')
		break;
	}
	if (count % 10 === 0){
		console.log('The number is divisible by 10. Skipping the number.');
		continue;
	}
	if (count % 5 === 0) {
		console.log(count);
		continue;
	}
}

// Without vowels
let longestWord = "supercalifragilisticexpialidocious";
let vowels = ["a", "e", "i", "o", "u"];
let longestEnglishWord = ""

for (let letter = 0; letter < longestWord.length; letter++) {
	if (
		longestWord[letter].toLowerCase() == "a" ||
		longestWord[letter].toLowerCase() == "e" ||
		longestWord[letter].toLowerCase() == "i" ||
		longestWord[letter].toLowerCase() == "o" ||
		longestWord[letter].toLowerCase() == "u"
	) {
		longestEnglishWord += "";
	}
	else {
		longestEnglishWord += longestWord[letter];
	}
}
console.log(longestWord);
console.log(longestEnglishWord);